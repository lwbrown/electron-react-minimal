import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  render() {
    return (
      <div>
        <h1>Hello, React in Electron!</h1>
        <button onClick={() => alert('This is working')}>Show Alert</button>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
