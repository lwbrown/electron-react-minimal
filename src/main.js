const { app, BrowserWindow } = require('electron');
const path = require('path');
const isDev = require('electron-is-dev');

// Global refrenence to window object
let mainWindow;

// Disable Security Warning
// @ts-ignore
process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = true;

//#region CreateWindow
function createWindow() {
  // Create the browser window
  mainWindow = new BrowserWindow({
    width: 600,
    height: 480,
    title: 'Passage',
    backgroundColor: '#eaeaea',
    show: false,
    webPreferences: {
      // Specifies a script that will be loaded before other scripts run
      // in the page. This script will always have access to node APIs no
      // matter whether node integration is turned on or off.
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
    },
  });

  // Load the index template html for window to show
  mainWindow.loadFile(path.join(__dirname, 'index.html'));

  // Hide Menu Bar for Production
  if (!isDev) mainWindow.setMenu(null);

  // On window close
  mainWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });

  // Show window when its ready
  mainWindow.on('ready-to-show', function() {
    mainWindow.show();
  });
}
//#endregion

//#region on ready
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);
// #endregion

//#region on window-all-closed
// Quit when all windows are closed.
app.on('window-all-closed', function() {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit();
});
//#endregion

//#region on activate
app.on('activate', function() {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow();
});
//#endregion
